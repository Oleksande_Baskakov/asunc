const arrOfCallbackForAfter = [];
const arrOfCallbackForError = [];

let isOneOfFuncWasCalled = false;

function done(succsesResult) {
    if (isOneOfFuncWasCalled) return;
    isOneOfFuncWasCalled = true;
    setTimeout(() => {
        let result = null;
        arrOfCallbackForAfter.forEach(el => {
            if(!result) {
                result = el(succsesResult);
            } else {
                result = el(result);
            }
        })
    }, 0)

}

function fail (err) {
    if (isOneOfFuncWasCalled) return;
    isOneOfFuncWasCalled = true;
    setTimeout(() => {
        arrOfCallbackForError.forEach(el => {
            el(err);
        })
    })

}

class DoAsync {
    constructor (func) {
        func(done, fail);
    }

    after (callback) {
        arrOfCallbackForAfter.push(callback);
        return this;
    }

    error (callback) {
        arrOfCallbackForError.push(callback);
        return this;
    }
}

const users = [{ name: 'Jack', name: 'Masha' }];

const getUser = new DoAsync(function(done, fail) {
  console.log(1);
  done(users);
  console.log(2);
  fail('Some Error');
  console.log(3);
  done(null);
})

console.log(4);

getUser.after(function(res) {
  console.log(8, res);
});

console.log(5);

getUser.error(function(err) {
  console.log('it has never been called!', err);
});

console.log(6);

getUser.after(function(res) {
  console.log(9, res);
});

console.log(7);

getUser
 .after(function(res) {
  console.log(9.1, res);
  return 10;
 })
 .after(function(res) {
  console.log(9.2, res);
 });

getUser
 .after(function(res) {
  console.log(10, res);
 })
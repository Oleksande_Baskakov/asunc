# Async Task
- Create a class - `DoAsync`

- Constructor takes 1 required argument - function;

- After creating an instance - the passed function will be executed immediately (synchronously)

- A function takes 2 arguments (optional) - these are two functions (e.g.: `done` and `fail`) 
and itself decide what exactly should call,
and transfer a successful result to done, and an error to fail.

- The constructor will return an instance to us,
which will have access to the `after` and `error` methods from the parent

- after и error can just register callbacks, one for each call

- If the `done` or `fail` methods was called, then the registered callbacks
will be called immediately after the completion of synchronous operations,
providing value or error to it;

- If method `done` was called, the call of `fail` must be ignored and vice versa. 
Those methods take into account only first call, the rest of all must be ignored!
Those methods take into account only first call, the rest of all must be ignored!

## How it should work:
```
const users = [{ name: 'Jack', name: 'Masha' }];

const getUser = new DoAsync(function(done, fail) {
  console.log(1);
  done(users);
  console.log(2);
  fail('Some Error');
  console.log(3);
  done(null);
})

console.log(4);

getUser.after(function(res) {
  console.log(8, res);
});

console.log(5);

getUser.error(function(err) {
  console.log('it has never been called!', err);
});

console.log(6);

getUser.after(function(res) {
  console.log(9, res);
});

console.log(7);

getUser
 .after(function(res) {
  console.log(9.1, res);
  return 10;
 })
 .after(function(res) {
  console.log(9.2, res);
 });

getUser
 .after(function(res) {
  console.log(10, res);
 })

```



### The execution result should be as follows:
```
1
2
3
4
5
6
7
8, [{ name: 'Jack', name: 'Masha' }]
9, [{ name: 'Jack', name: 'Masha' }]
// 9.1 [{...},{...}]
// 9.2 10
// 10 [{...},{...}]
```